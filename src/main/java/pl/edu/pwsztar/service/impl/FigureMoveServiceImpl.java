package pl.edu.pwsztar.service.impl;

import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.FigureMoveService;

import java.util.Arrays;
import java.util.List;

@Service
public class FigureMoveServiceImpl implements FigureMoveService {

    @Override
    public Boolean bishopMove(FigureMoveDto figureMoveDto) {
        int startRow;
        int startColumn;
        int destinationRow;
        int destinationColumn;

        List<String> startPosition = Arrays.asList(figureMoveDto.getStart().split("_"));
        List<String> destinationPosition = Arrays.asList(figureMoveDto.getDestination().split("_"));

        startRow = Integer.parseInt(startPosition.get(1));
        destinationRow = Integer.parseInt(destinationPosition.get(1));

        startColumn = columnNameToInteger(startPosition.get(0));
        destinationColumn = columnNameToInteger(destinationPosition.get(0));

        return (Math.abs(startRow - destinationRow) == Math.abs(startColumn - destinationColumn));
    }

    private int columnNameToInteger(String columnName){
        return columnName.charAt(0) - 96;
    }
}
