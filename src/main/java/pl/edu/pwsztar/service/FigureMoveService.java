package pl.edu.pwsztar.service;

import pl.edu.pwsztar.domain.dto.FigureMoveDto;

public interface FigureMoveService {
    Boolean bishopMove(FigureMoveDto figureMoveDto);
}
